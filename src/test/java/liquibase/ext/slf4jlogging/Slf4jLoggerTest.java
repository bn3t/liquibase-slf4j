/*
 * $Log$ 
 */
package liquibase.ext.slf4jlogging;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import liquibase.logging.LogLevel;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Bernard
 *
 */
public class Slf4jLoggerTest {
    
    @Mock private Logger logger;
    
    @InjectMocks Slf4jLogger slf4jLogger = new Slf4jLogger();
    
    @BeforeMethod
    public void init() {
	MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getPriority() {
	int actual = slf4jLogger.getPriority();
	Assert.assertEquals(actual, 5);
    }
    
    @Test
    public void setName() {
	slf4jLogger.setName("myname");
//	Assert.assertEquals(logger.logger.getName(), "myname");
    }
    
    @Test
    public void severe() {
	slf4jLogger.severe("mymessage");
	verify(logger).error(eq("mymessage"));
    }
    @Test
    public void severe_throwable() {
	slf4jLogger.severe("mymessage", new Throwable());
	verify(logger).error(eq("mymessage"), any(Throwable.class));
    }
    
    @Test
    public void warning() {
	slf4jLogger.warning("mymessage");
	verify(logger).warn(eq("mymessage"));
    }
    @Test
    public void warning_throwable() {
	slf4jLogger.warning("mymessage", new Throwable());
	verify(logger).warn(eq("mymessage"), any(Throwable.class));
    }
    
    @Test
    public void info() {
	slf4jLogger.info("mymessage");
	verify(logger).info(eq("mymessage"));
    }
    @Test
    public void info_throwable() {
	slf4jLogger.info("mymessage", new Throwable());
	verify(logger).info(eq("mymessage"), any(Throwable.class));
    }
    
    @Test
    public void debug() {
	slf4jLogger.debug("mymessage");
	verify(logger).debug(eq("mymessage"));
    }
    @Test
    public void debug_throwable() {
	slf4jLogger.debug("mymessage", new Throwable());
	verify(logger).debug(eq("mymessage"), any(Throwable.class));
    }
    @Test
    public void setLogLevel_LogLevel() {
	slf4jLogger.setLogLevel((LogLevel)null);
    }
    @Test
    public void setLogLevel_String() {
	slf4jLogger.setLogLevel("loglevel");
    }
    @Test
    public void setLogLevel_String_String() {
	slf4jLogger.setLogLevel("loglevel", "logfile");
    }
    @Test
    public void getLogLevel() {
	slf4jLogger.getLogLevel();
    }
}
